/**
 * @file
 * javascript functions to control form fields.
 */
(function ($) {
  $(document).ready(function() {
    $('.stockquote').each(function() {
      var $this = $(this);
      var $type = $(this).data('type');
      var $prefix = $(this).data('prefix');
      var $url = $(this).data('url');

      $this.addClass('loading');

      // Get the json data
      $.getJSON($url, function(data) {
        $this.removeClass('loading');

        if(data) {
          $($this).html($('#StockQuote' + $type + 'Template' + $prefix).tmpl(data));
        } else {
          $($this).find('.stockquote-alternate-message').show();
        }
      });
    });
  });
})(jQuery);
