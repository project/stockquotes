<?php
/**
 * @file
 * Template to display stock quote information.
 **/
?>
<div class="stockquote-wrap">
  <div class="stockquote-info">
    <?php if(!empty($keys)): ?>
      <?php foreach($keys as $key => $label): ?>
        <div class="stockquote-<?php echo $key; ?>">
          <label><?php echo t($label); ?></label> <span>${<?php echo $key; ?>}</span>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>
  <div class="stockquote-disclaimer">
    <p>Stock quote is delayed up to 15 mins. Please refer to <a href="http://tmxmoney.com" target="_blank">tmxmoney.com</a> for a live stock quote.</p>
  </div>
</div>
