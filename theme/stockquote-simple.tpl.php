<?php
/**
 * @file
 * Template to display stock quote information.
 **/
?>
<div class="stockquote-wrap">
  <div class="stockquote-info">
    <label>${latest_price}</label> ${change} (${percent_change})
  </div>
  <div class="stockquote-disclaimer">
    <p>Stock quote is delayed up to 15 mins. Please refer to <a href="http://tmxmoney.com" target="_blank">tmxmoney.com</a> for a live stock quote.</p>
  </div>
</div>
