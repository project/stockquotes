
Stock Quote 7.x-1.0
--------------------------

Install
-------
* Enable the module
* In the Administration dashboard, go to Configuration > Web Services > Stock Quote settings to change settings
* Don't forget to set permissions, otherwise nobody will see the changes
* Go to Structure > Blocks and configure the Stock Quote block where you want it to appear

Description
-----------
This module creates a block to display stock information through the Alpha Vantage API.

Author
------
Marco Maranao (marcom@upanup.com)
Upanup Studios (https://upanup.com)
