<?php
  /**
   * @file
   * Stock Quote admin.
   */

  /**
   * Form builder for stockquote settings.
   */
  function stockquote_settings_form($form, &$form_state) {
    $form['api'] = array(
      '#type' => 'fieldset',
      '#title' => t('API Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['api']['stockquote_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#default_value' => variable_get('stockquote_api_key', ''),
      '#description' => t('Claim your free API key with lifetime access from <a href="https://www.alphavantage.co/support/#api-key" target="_blank">https://www.alphavantage.co/support/#api-key</a>.'),
      '#required' => TRUE
    );

    $form = system_settings_form($form);

    // Add custom submit handler
    array_unshift($form['#submit'], 'stockquote_settings_form_submit');

    return $form;
  }

  /**
   * Implements hook_form_FORM_ID_alter() for stockquote_form_settings().
   */
  function stockquote_form_stockquote_settings_form_alter(&$form, &$form_state) {
    /*$api_password = variable_get('stockquote_api_password');

    if (!empty($api_password)) {
      // Remove password requirement if password is already saved
      unset($form['api']['stockquote_api_password']['#required']);
    }*/
  }

  /**
   * Form validation handler for stockquote_settings_form().
   *
   * @see stockquote_settings_form_submit()
   */
  function stockquote_settings_form_validate($form, &$form_state) {
    //TODO: Use API to test retrieving data
    // Get settings
    /*$endpoint = $form_state['values']['stockquote_api_endpoint'];
    $token = $form_state['values']['stockquote_api_token'];
    $password = $form_state['values']['stockquote_api_password'];
    $newsletter_id = $form_state['values']['stockquote_newsletter_id'];

    $upaknee = new Upaknee($endpoint, $token, $password);

    $response = $upaknee->getNewsletter($newsletter_id);

    if ($response['code'] != 200) {
      form_set_error('form', $response['message']);
    }*/
  }

  /**
   * Form submission handler for stockquote_settings_form().
   *
   * @see stockquote_settings_form_validate()
   */
  function stockquote_settings_form_submit($form, &$form_state) {
    //TODO: Probably not needed
    // Do not save the password value if empty.
    /*if ($form_state['values']['stockquote_api_password'] == '') {
      unset($form_state['values']['stockquote_api_password']);
    }*/
  }
